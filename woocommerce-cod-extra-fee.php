<?php
/**
 * Plugin Name: WooCommerce Utánvétes fizetési mód extra díj
 * Plugin URI: https://www.brandcomstudio.com
 * Description: add extra fee on cash on delivery (COD) payment method
 * Version: 2021.11.13.
 * Author: Harkály Gergő
 * Author URI: https://www.harkalygergo.hu
 * Useful: https://github.com/WPPlugins/woocommerce-cod-advanced/blob/master/woocommerce_advanced_cod.php
*/

// prevent direct access
defined( 'ABSPATH' ) || exit;

class WooCommerceCodExtraFee
{
	private $fee;
	private $fee_tax;
	private $fee_taxable;
	private $fee_name;

	public function __construct()
	{
		if( class_exists( 'WC_Gateway_COD' ) )
		{
			$WC_Gateway_COD = new WC_Gateway_COD;
			$this->fee = $WC_Gateway_COD->settings['cod_fee'];
			$this->fee_tax = 0;
			$this->fee_taxable = false;
			if( $WC_Gateway_COD->settings['cod_fee_tax']!=="" )
			{
				$this->fee_tax = $WC_Gateway_COD->settings['cod_fee_tax'];
				$this->fee_taxable = true;
			}
			$this->fee_name = $WC_Gateway_COD->get_title();
		}

		add_action( 'woocommerce_settings_api_form_fields_cod' , array( $this,'action_woocommerce_settings_api_form_fields_cod' ));
		add_action( 'woocommerce_cart_calculate_fees', array( $this, 'action_woocommerce_cart_calculate_fees'), 20, 1 );
		add_action( 'wp_footer', array( $this,'action_wp_footer') );
	}

	// COD admin options
	public function action_woocommerce_settings_api_form_fields_cod($form_fields)
	{
		$form_fields['cod_fee'] = array(
			'title'			=> 'Nettó díj',
			'type'			=> 'number',
			'description'	=> 'nettó összeg, például: 400 | csak számokat adjon meg',
			'default'		=> '400',
		);
		$form_fields['cod_fee_tax'] = array(
			'title'			=> 'Utánvét díj áfakulcsa',
			'type'			=> 'number',
			'description'	=> 'ha áfamentes, hagyja üresen egyéb esetben adja meg az áfakulcsot, például: 27 | csak számokat adjon meg',
			'default'		=> '27',
		);

		return $form_fields;
	}

	// add a custom fee based o cart subtotal
	public function action_woocommerce_cart_calculate_fees( $cart )
	{
		if ( is_admin() && ! defined( 'DOING_AJAX' ) )
			return;

		if ( ! ( is_checkout() && ! is_wc_endpoint_url() ) )
			return; // only checkout page

		$payment_method = WC()->session->get( 'chosen_payment_method' );
		if ( $payment_method  === 'cod')
		{
			$cart->add_fee( $this->fee_name , $this->fee, $this->fee_taxable, $this->fee_tax );
		}
	}

	// jQuery - Update checkout on methode payment change
	public function action_wp_footer()
	{
		if ( ! ( is_checkout() && ! is_wc_endpoint_url() ) )
			return; // only checkout page
		?>
		<script type="text/javascript">
			jQuery( function($){
				$('form.checkout').on('change', 'input[name="payment_method"]', function(){
					$(document.body).trigger('update_checkout');
				});
			});
		</script>
		<?php
	}
}

// load our class after plugins loaded and WooCommerce is activated
add_action('plugins_loaded', function()
	{
		if( defined('WC_VERSION') )
		{
			new WooCommerceCodExtraFee();
		}
	}
);